# Quirk Selector

Slipways mod to allow the selection of quirks to take into a new standard or endless run.

All sector types and quirks available in the game (as at Slipways v1.0 [b697 / b698]) are available for selection.

The mod should be able to handle all conflicting quirks gracefully; quirks will be locked (preventing selection) when other conflicting
quirks are selected.

Note that this mod (by default) disables the ability to randomise the sector by clicking on the sector map; this can be disabled in the
mod's configuration (F1 by default if Configuration Manager is installed), or the dedicated randomise button can be used instead.

_No pre-compiled releases are available at this time._

## Ranked and Campaign modes

This mod will not activate when accessing the Ranked and Campaign modes, and so is safe to have installed when competing on the leaderboard.

# Installation instructions

_No installation instructions for MacOS are available at this time; not sure if the mod will even work on MacOS._

## Windows

1. Build the mod from source using the instructions below
2. (Optional) Download and install the latest release of [Configuration Manager](https://github.com/BepInEx/BepInEx.ConfigurationManager)
   into your Slipways installation directory.

# Build instructions

_No build instructions for MacOS are available at this time; not sure if the mod will even work on MacOS._

## Windows

1. Download and install Slipways from your chosen source
2. Download and install the latest stable x64 release of [BepInEx 5](https://docs.bepinex.dev/articles/user_guide/installation/index.html)
   into your Slipways installation directory
3. Download and install the [.NET Framework 4.6.2 Developer Pack](https://dotnet.microsoft.com/download/dotnet-framework/net462)
4. Set `SLIPWAYS_HOME` environment variable to your Slipways installation directory
    - Should contain `Slipways.exe` inside
5. Start your IDE of choice, or restart it if it was already running
    - Note that in the case of Rider, `File -> Invalidate Caches... -> Just restart` is not sufficient for it to pick up the new environment
      variable
6. Build the solution using your IDE

Apart from compiling the `.dll` itself, building also:

- Copies the necessary assemblies from the game directory
- Copies the compiled `.dll` to the game directory, ready for testing
