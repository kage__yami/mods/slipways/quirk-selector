using System;

namespace QuirkSelector.Data
{
    public enum GameMode
    {
        Standard,
        Endless,
        Campaign,
        Ranked
    }

    public static class GameModeExtensions
    {
        public static GameMode AsGameMode(this string mode) => mode switch {
            "standard" => GameMode.Standard,
            "sandbox" => GameMode.Endless,
            "campaign" => GameMode.Campaign,
            "ranked" => GameMode.Ranked,
            _ => throw new ArgumentOutOfRangeException(nameof(mode), mode, "must be one of: standard, sandbox, campaign, ranked")
        };
    }
}
