using System.Collections.Generic;

namespace QuirkSelector.Data.Mutators
{
    public record TeemingWithLife : Mutator
    {
        public override string LabelId => "mutator.no_bad";

        public override string Label => "Teeming with life";

        public override string DescriptionId => "mutator.no_bad.desc";

        public override object[]? DescriptionParams => null;


        public override string Description => "This sector contains no uninhabitable planets.";

        public override MutatorKind Kind => MutatorKind.Composition;

        public override bool Hidden => false;

        public override int? ScoreModifier => -15;

        public override List<string>? Packages => null;

        public override List<string> Conditions => new() {"MapGenNoBadPlanets()"};

        public override List<string>? Effects => null;
    }
}
