using System.Collections.Generic;

namespace QuirkSelector.Data.Mutators
{
    public record Hospitable : Mutator
    {
        public override string LabelId => "mutator.hospitable";

        public override string Label => "Hospitable";

        public override string DescriptionId => "mutator.more_planets_of_one_type.desc";

        public override object[] DescriptionParams => new object[] {"planet.earthlike"};


        public override string Description => "This sector has an altered composition, with significantly more [[ref:{1}]] planets.";

        public override MutatorKind Kind => MutatorKind.Composition;

        public override bool Hidden => false;

        public override int? ScoreModifier => -10;

        public override List<string>? Packages => null;

        public override List<string> Conditions => new() {"MapGenGuaranteeEarth()"};

        public override List<string>? Effects => null;
    }
}
