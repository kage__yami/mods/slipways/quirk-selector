using System.Collections.Generic;
using Slipways.General.Localizations;

namespace QuirkSelector.Data.Mutators
{
    public record MiningFocus : Mutator
    {
        public override string LabelId => "mutator.score_for_mining";

        public override string Label => "Mining focus";

        public override string DescriptionId => "mutator.scoring_replacement.desc";

        public override object[] DescriptionParams => new object[] {
            new LocalizedString("scoring.mut.production.desc",
                "Each unit of {1} produced is worth *{2}:star:*.",
                ":O:", 50
            )
        };


        public override string Description => "Replaces the scoring for empire size with the following rule:\n{1}";

        public override MutatorKind Kind => MutatorKind.ScoreReplacing;

        public override bool Hidden => false;

        public override int? ScoreModifier => null;

        public override List<string>? Packages => null;

        public override List<string> Conditions => new() {
            "MutReplaceEmpireSizeScoring(MutScoringProduction('mutator.score_for_mining', 'O', 50))"
        };

        public override List<string>? Effects => null;
    }
}
