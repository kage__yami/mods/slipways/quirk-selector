using System.Collections.Generic;

namespace QuirkSelector.Data.Mutators
{
    public record AnomalousSector : Mutator
    {
        public override string LabelId => "mutator.sector.anomalies";

        public override string Label => "Anomalous sector";

        public override string DescriptionId => "mutator.sector.anomalies.desc";

        public override object[]? DescriptionParams => null;

        public override string Description => "This sector contains anomalies that limit your view, but are also your best route to getting :S:.";

        public override MutatorKind Kind => MutatorKind.SectorType;

        public override bool Hidden => false;

        public override int? ScoreModifier => 10;

        public override List<string> Packages => new() {"modes/campaign-diaspora/m-dark/st-dark"};

        public override List<string>? Conditions => null;

        public override List<string>? Effects => null;
    }
}
