using System.Collections.Generic;
using Slipways.General.Localizations;

namespace QuirkSelector.Data.Mutators
{
    public record UnstablePlanets : Mutator
    {
        public override string LabelId => "mutator.unstable_planets";

        public override string Label => "Unstable planets";

        public override string DescriptionId => "mutator.planets_affected.desc";

        public override object[] DescriptionParams => new object[] {
            new LocalizedString("quirk.unstable_core", "Unstable core"),
            new LocalizedString("quirk.unstable_core.desc",
                "This planet cannot produce or receive :P:, but the unstable core can be used to manufacture energy (:B::arrow::E:)."
            )
        };

        public override string Description => "Many planets in this sector have the following quality:\n*{1}*: {2}";

        public override MutatorKind Kind => MutatorKind.Quirk;

        public override bool Hidden => false;

        public override int? ScoreModifier => 0;

        public override List<string>? Packages => null;

        public override List<string> Conditions => new() {"PlanetQuirkAdder('QuirkUnstableCore()', 20)"};

        public override List<string>? Effects => null;
    }
}
