using System.Collections.Generic;
using Slipways.General.Localizations;

namespace QuirkSelector.Data.Mutators
{
    public record IndustryFocus : Mutator
    {
        public override string LabelId => "mutator.score_for_forgeworlds";

        public override string Label => "Industry focus";

        public override string DescriptionId => "mutator.scoring_replacement.desc";

        public override object[] DescriptionParams => new object[] {
            new LocalizedString("scoring.mut.successful_of_type.desc",
                "Each [[ref:planet.{1}]] planet that is [[ref:level.2]] or better is worth *{2}:star:*.",
                "factory", 350
            )
        };


        public override string Description => "Replaces the scoring for empire size with the following rule:\n{1}";

        public override MutatorKind Kind => MutatorKind.ScoreReplacing;

        public override bool Hidden => false;

        public override int? ScoreModifier => null;

        public override List<string>? Packages => null;

        public override List<string> Conditions => new() {
            "MutReplaceEmpireSizeScoring(MutScoringSuccessfulOfType('mutator.score_for_forgeworlds', 'factory', 350))"
        };

        public override List<string>? Effects => null;
    }
}
