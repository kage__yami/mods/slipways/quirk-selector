using System.Collections.Generic;
using Slipways.General.Localizations;

namespace QuirkSelector.Data.Mutators
{
    public record BeautifulSights : Mutator
    {
        public override string LabelId => "quirk.beautiful_sights";

        public override string Label => "Beautiful sights";

        public override string DescriptionId => "mutator.planets_affected.desc";

        public override object[] DescriptionParams => new object[] {
            new LocalizedString("quirk.beautiful_sights", "Beautiful sights"),
            new LocalizedString("quirk.beautiful_sights.desc",
                "If you settle :P: on this planet and upgrade it to [[ref:level.2]], it will yield an additional [[delta:{1}H]].",
                2
            )
        };


        public override string Description => "Many planets in this sector have the following quality:\n*{1}*: {2}";

        public override MutatorKind Kind => MutatorKind.Quirk;

        public override bool Hidden => false;

        public override int? ScoreModifier => -5;

        public override List<string>? Packages => null;

        public override List<string> Conditions => new() {"PlanetQuirkAdder('QuirkSights()', 12)"};

        public override List<string>? Effects => null;
    }
}
