using System.Collections.Generic;

namespace QuirkSelector.Data.Mutators
{
    public record DecrepitRuins : Mutator
    {
        public override string LabelId => "mutator.old_ruins";

        public override string Label => "Decrepit ruins";

        public override string DescriptionId => "mutator.old_ruins.desc";

        public override object[]? DescriptionParams => null;


        public override string Description => "It is not possible to make :B: on [[ref:planet.remnant]] planets.";

        public override MutatorKind Kind => MutatorKind.Limitation;

        public override bool Hidden => false;

        public override int? ScoreModifier => 10;

        public override List<string>? Packages => null;

        public override List<string>? Conditions => null;

        public override List<string> Effects => new() {"quality(MutOldRuins())"};
    }
}
