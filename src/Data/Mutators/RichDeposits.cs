using System.Collections.Generic;

namespace QuirkSelector.Data.Mutators
{
    public record RichDeposits : Mutator
    {
        public override string LabelId => "mutator.rich_deposits";

        public override string Label => "Rich deposits";

        public override string DescriptionId => "mutator.rich_deposits.desc";

        public override object[]? DescriptionParams => null;


        public override string Description => "Mines on *mineral planets* yield one additional :O:.";

        public override MutatorKind Kind => MutatorKind.Positive;

        public override bool Hidden => false;

        public override int? ScoreModifier => -5;

        public override List<string>? Packages => null;

        public override List<string>? Conditions => null;

        public override List<string> Effects => new() {"quality(RichDepositsQuality())"};
    }
}
