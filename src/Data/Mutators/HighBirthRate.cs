using System.Collections.Generic;

namespace QuirkSelector.Data.Mutators
{
    public record HighBirthRate : Mutator
    {
        public override string LabelId => "mutator.high_birth_rate";

        public override string Label => "High birth rate";

        public override string DescriptionId => "mutator.high_birth_rate.desc";

        public override object[]? DescriptionParams => null;


        public override string Description => "Planets and structures that make :P: make one more :P: when all needs are met.";

        public override MutatorKind Kind => MutatorKind.Improvement;

        public override bool Hidden => false;

        public override int? ScoreModifier => -5;

        public override List<string>? Packages => null;

        public override List<string>? Conditions => null;

        public override List<string> Effects => new() {"quality(MutHighBirthRate())"};
    }
}
