using System.Collections.Generic;

namespace QuirkSelector.Data.Mutators
{
    public record WeakSlipspace : Mutator
    {
        public override string LabelId => "mutator.weak_slipspace";

        public override string Label => "Weak slipspace";

        public override string DescriptionId => "mutator.weak_slipspace.desc";

        public override object[] DescriptionParams => new object[] {15};


        public override string Description => "Slipspace overload starts *{1}% sooner*.";

        public override MutatorKind Kind => MutatorKind.Negative;

        public override bool Hidden => false;

        public override int? ScoreModifier => 10;

        public override List<string>? Packages => null;

        public override List<string>? Conditions => null;

        public override List<string> Effects => new() {"reduce(slipspace_overload.starts_at, 15%)"};
    }
}
