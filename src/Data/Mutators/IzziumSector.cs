using System.Collections.Generic;

namespace QuirkSelector.Data.Mutators
{
    public record IzziumSector : Mutator
    {
        public override string LabelId => "mutator.sector.izzium";

        public override string Label => "Izzium sector";

        public override string DescriptionId => "mutator.sector.izzium.desc";

        public override object[]? DescriptionParams => null;

        public override string Description =>
            "This sector contains planets with *unstable izzium deposits*. Such planets are uninhabitable until the izzium is stabilized.";

        public override MutatorKind Kind => MutatorKind.SectorType;

        public override bool Hidden => false;

        public override int? ScoreModifier => null;

        public override List<string> Packages => new() {"modes/campaign-diaspora/m-izzium/st-izzium"};

        public override List<string>? Conditions => null;

        public override List<string>? Effects => null;
    }
}
