using System.Collections.Generic;
using Slipways.General.Localizations;

namespace QuirkSelector.Data.Mutators
{
    public record AbundantResources : Mutator
    {
        public override string LabelId => "quirk.abundant_resources";

        public override string Label => "Abundant resources";

        public override string DescriptionId => "mutator.planets_affected.desc";

        public override object[] DescriptionParams => new object[] {
            new LocalizedString("quirk.abundant_resources", "Abundant resources"),
            new LocalizedString("quirk.abundant_resources.desc",
                "As long as this planet is making :O:, :L: or :W:, it earns an extra [[delta:{1}$]] for each export.",
                2
            )
        };


        public override string Description => "Many planets in this sector have the following quality:\n*{1}*: {2}";

        public override MutatorKind Kind => MutatorKind.Quirk;

        public override bool Hidden => false;

        public override int? ScoreModifier => -5;

        public override List<string>? Packages => null;

        public override List<string> Conditions => new() {"PlanetQuirkAdder('QuirkAbundantResources()', 15)"};

        public override List<string>? Effects => null;
    }
}
