using System.Collections.Generic;

namespace QuirkSelector.Data.Mutators
{
    public record HarmfulRadiation : Mutator
    {
        public override string LabelId => "mutator.radiation";

        public override string Label => "Harmful radiation";

        public override string DescriptionId => "mutator.radiation.desc";

        public override object[] DescriptionParams => new object[] {2};


        public override string Description =>
            "Many habitable planets in this sector are irradiated, causing {1}:$: extra upkeep when settled with :P:.";

        public override MutatorKind Kind => MutatorKind.Quirk;

        public override bool Hidden => false;

        public override int? ScoreModifier => 5;

        public override List<string>? Packages => null;

        public override List<string> Conditions => new() {"PlanetQuirkAdder('QuirkIrradiated()', 35)"};

        public override List<string>? Effects => null;
    }
}
