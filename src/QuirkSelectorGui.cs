using System.Collections.Generic;
using System.Linq;
using QuirkSelector.Data;
using Slipways.Main.Empires.Mutators;
using UnityEngine;

namespace QuirkSelector
{
    internal partial class QuirkSelector
    {
        internal static bool DisplayQuirkSelector;
        internal static bool ResetQuirkSelection;

        private Rect _quirkSelectorWindow = new(50, 50, Screen.width / 4f, Screen.height - 200);
        private Vector2 _quirkSelectorScrollPos;

        private bool _areGuiStylesSetup;
        private GUIStyle? _wordWrappedToggleStyle;

        /// <summary>
        /// Collection of mutators showing as selected in the GUI.
        /// </summary>
        private readonly Dictionary<Mutator, bool> _selected = Mutator.InitialiseDictionary<bool>();

        /// <summary>
        /// Non-authoritative collection of mutators added to the sector already.
        /// </summary>
        private readonly Dictionary<Mutator, bool> _added = Mutator.InitialiseDictionary<bool>();


        private void OnGUI()
        {
            if (!_areGuiStylesSetup) {
                SetupGuiStyles();
            }

            if (DisplayQuirkSelector) {
                ResetSelectionsIfNeeded();

                DisplayGui();
                HandleSelections();
            }
        }

        private void SetupGuiStyles()
        {
            _areGuiStylesSetup = true;

            _wordWrappedToggleStyle = GUI.skin.toggle;
            _wordWrappedToggleStyle.wordWrap = true;
        }

        // ReSharper disable Unity.PerformanceAnalysis - does not run every frame as the function immediately disables
        // itself once executed.
        private void ResetSelectionsIfNeeded()
        {
            if (ResetQuirkSelection) {
                ResetQuirkSelection = false;

                foreach (Mutator mutator in Mutator.Variants()) {
                    _selected[mutator] = false;
                    _added[mutator] = false;
                }

                foreach (MutatorSpec spec in Slipways.Mutators()) {
                    _selected[spec] = true;
                    _added[spec] = true;
                }
            }
        }

        private void DisplayGui()
        {
            _quirkSelectorWindow = GUI.Window(0, _quirkSelectorWindow, _ => {
                    _quirkSelectorScrollPos = GUILayout.BeginScrollView(_quirkSelectorScrollPos);
                    {
                        foreach (Mutator mutator in Mutator.Variants()) {
                            GUI.enabled = !mutator.AreConflictsSelected(_selected);
                            {
                                _selected[mutator] = GUILayout.Toggle(
                                    GUI.enabled && _selected[mutator], $"{mutator.Label}:\n{mutator.LocalisedDescription()}",
                                    _wordWrappedToggleStyle
                                );
                            }
                        }

                        GUI.enabled = true;
                    }
                    GUILayout.EndScrollView();

                    GUI.DragWindow(new(0, 0, _quirkSelectorWindow.width, 20));
                }, "Quirk Selector"
            );
        }

        // ReSharper disable Unity.PerformanceAnalysis - does not run every frame as it only runs if a mutator is
        // enabled or disabled in the last frame.
        private void HandleSelections()
        {
            foreach (Mutator mutator in Mutator.Variants().Where(mutator => _selected[mutator] != _added[mutator])) {
                _added[mutator] = _selected[mutator];

                List<MutatorSpec> mutators = Slipways.Mutators();

                if (_selected[mutator]) {
                    mutators.Add(mutator);
                } else {
                    mutators.RemoveAll(spec => spec.Label.ID == mutator.LabelId);
                }
            }
        }
    }
}
