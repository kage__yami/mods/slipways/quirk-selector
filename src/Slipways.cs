using System.Collections.Generic;
using HarmonyLib;
using Slipways.General.GameData;
using Slipways.General.GameData.Seeds;
using Slipways.Main.Empires.Mutators;
using Slipways.Toplevel.Preps;
using UnityEngine;

namespace QuirkSelector
{
    public static class Slipways
    {
        public static GameConfig GameConfig() =>
            ((Preparations)AccessTools
                .Property(typeof(PreparationScene), "Preparations")
                .GetValue(GameObject.Find("Root").GetComponent<PreparationScene>())
            ).GameConfig;

        public static List<MutatorSpec> Mutators() =>
            (List<MutatorSpec>)AccessTools
                .Field(typeof(SectorSpec), "_mutators")
                .GetValue(GameConfig().Sector);
    }
}
