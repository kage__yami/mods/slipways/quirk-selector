using BepInEx.Configuration;

namespace QuirkSelector
{
    internal class QuirkSelectorConfig
    {
        private readonly ConfigEntry<bool> _disableSectorMapRandomisation;
        internal bool DisableSectorMapRandomisation => _disableSectorMapRandomisation.Value;

        internal QuirkSelectorConfig(QuirkSelector plugin)
        {
            _disableSectorMapRandomisation = plugin.Config.Bind("General", "Disable Sector Map Randomisation?", true,
                "Should clicking the sector map trigger sector randomisation? (Note: the dedicated randomise button will still function when this is disabled.)"
            );
        }
    }
}
