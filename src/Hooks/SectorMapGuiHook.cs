using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using HarmonyLib;
using QuirkSelector.Data;
using Slipways.General.UI.BlockLists;
using Slipways.General.UI.BlockLists.Blocks;
using Slipways.Main.Empires.Mutators.Views;
using Slipways.Main.Technologies.Views;
using Slipways.Toplevel.Preps.Views.Galaxies;

namespace QuirkSelector.Hooks
{
    [SuppressMessage("ReSharper", "InconsistentNaming",
        Justification = "Harmony uses magic parameter names for hooking into the game's function."
    )]
    internal static class SectorMapGuiHook
    {
        /// <summary>
        /// Called after drawing the sector details GUI on the sector map.
        /// </summary>
        /// <remarks>
        /// Used to remove the quirk details section.
        /// </remarks>
        [HarmonyPatch(typeof(VSeedSelection), "GenerateContents")]
        [HarmonyPostfix]
        private static IEnumerable<IUIBlock> OnDrawSectorDetailsGui(IEnumerable<IUIBlock> contents, VSeedSelection __instance)
        {
            if (Slipways.GameConfig().GameMode.AsGameMode() is GameMode.Standard or GameMode.Endless
                && __instance.Target.AllowQuirks
            ) {
                bool separatorYielded = false;
                bool spaceYielded = false;

                foreach (IUIBlock content in contents.Where(content => content is not BMutator)) {
                    if (content is BTechLevelSeparator or BSpace) {
                        switch (content) {
                            case BTechLevelSeparator when !separatorYielded:
                                separatorYielded = true;
                                yield return content;
                                break;
                            case BSpace when !spaceYielded:
                                spaceYielded = true;
                                yield return content;
                                break;
                        }
                    } else {
                        yield return content;
                    }
                }
            } else {
                foreach (IUIBlock content in contents) {
                    yield return content;
                }
            }
        }

        /// <summary>
        /// Called after toggling quirks on or off.
        /// </summary>
        /// <remarks>
        /// Used to show and hide the quirk selector.
        /// </remarks>
        [HarmonyPatch(typeof(UIMSeedSelection), nameof(UIMSeedSelection.ToggleQuirks))]
        [HarmonyPostfix]
        private static void OnToggleQuirks(UIMSeedSelection __instance)
        {
            QuirkSelector.DisplayQuirkSelector = __instance.AllowQuirks;
        }
    }
}
