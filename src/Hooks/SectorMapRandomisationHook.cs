using System.Diagnostics.CodeAnalysis;
using HarmonyLib;
using Slipways.General.Audio;
using Slipways.General.Cursors;
using Slipways.Toplevel.Preps.Views.Galaxies;
using UnityEngine;

namespace QuirkSelector.Hooks
{
    [SuppressMessage("ReSharper", "InconsistentNaming",
        Justification = "Harmony uses magic parameter names for hooking into the game's function."
    )]
    internal static class SectorMapRandomisationHook
    {
        /// <summary>
        /// Called when clicking/dragging on the sector map.
        /// </summary>
        /// <remarks>
        /// Normally this triggers sector randomisation, but instead we're skipping it entirely (returning <c>false</c>)
        /// if configured to do so.
        /// </remarks>
        [HarmonyPatch(typeof(VSeedMap), "WhenDraggedTo", typeof(Vector2))]
        [HarmonyPrefix]
        private static bool BeforeSectorMapFinishedDrag() => !QuirkSelector.Conf.DisableSectorMapRandomisation;

        /// <summary>
        /// Called when the sound cue is triggered when clicking/dragging on the sector map.
        /// </summary>
        /// <remarks>
        /// Used to skip the sound that plays when clicking/dragging on the sector map (when sector map randomisation is
        /// disabled).
        /// </remarks>
        [HarmonyPatch(typeof(SoundRequest), nameof(SoundRequest.Play))]
        [HarmonyPrefix]
        private static bool BeforeSectorMapFinishedDragSoundCue(SoundCue ___cue, string ___exclusionGroup, int ___exclusionPriority) =>
            !(
                QuirkSelector.Conf.DisableSectorMapRandomisation
                && ___cue == SoundCue.ButtonHover
                && ___exclusionGroup == "seed_change"
                && ___exclusionPriority == 0
            );

        /// <summary>
        /// Called after the cursor type to display on the sector map is retrieved.
        /// </summary>
        /// <remarks>
        /// Used to disable the hand-style cursor when clicking/dragging on the sector map (when sector map
        /// randomisation is disabled).
        /// </remarks>
        [HarmonyPatch(typeof(VSeedMap), nameof(VSeedMap.Cursor), MethodType.Getter)]
        [HarmonyPostfix]
        private static MouseCursor OnSectorMapDisplayCursor(MouseCursor cursor) =>
            QuirkSelector.Conf.DisableSectorMapRandomisation ? MouseCursor.DefaultArrow : cursor;
    }
}
